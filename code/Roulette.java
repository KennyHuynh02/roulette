import java.util.Scanner;
class Roulette{
    public static void main(String[] args){
        
        //roulette wheel object
        RouletteWheel rouletteWheel = new RouletteWheel();
        boolean answer = true;

        //loop of the game
        loopGame(rouletteWheel, answer);
        
    }

    //loop of the game helpermethod
    private static void loopGame(RouletteWheel rouletteWheel, boolean answer){
        while(answer){
            //greeting the user
            answer = greeting();

            if(answer){
            //asking for betting amount   
            betting(rouletteWheel);
            
            //spinning the wheel
            spinTheWheel(rouletteWheel);
            }

            if(rouletteWheel.getBetMoney()==0){
                answer=false;
            }
        }
    }

    //Greets the user with a message
    public static boolean greeting(){
        Scanner keyboard = new Scanner(System.in);
        System.out.println("Welcome to Roulette Wheel!");
        System.out.println("Would you like to bet? (yes) to play (any other letter(s)) to not play");
        String userInput = keyboard.next();
        
        if(userInput.equals("yes")){
            return true;
        }
        return false;
    }

    //Asks the user for a betting amount
    public static void betting(RouletteWheel rouletteWheel){
        Scanner keyboard = new Scanner(System.in);
        boolean confirmation = false;

        while(!confirmation){
            System.out.println("remaining betMoney: " + rouletteWheel.getBetMoney());
            System.out.println("insert a correct betting amount...");
            double bettingAmount = keyboard.nextDouble();
            boolean answer = betAmountValidation(rouletteWheel, bettingAmount);
            confirmation = answer;
        }
    }

    //helper method for the betting method
    private static boolean betAmountValidation(RouletteWheel rouletteWheel, double bettingAmount){
        if(bettingAmount <= 0){
            rouletteWheel.setBettingAmount(bettingAmount);
            return false;
        }
        
        if(rouletteWheel.getBetMoney() >= bettingAmount){
            rouletteWheel.setBettingAmount(bettingAmount);
            return true;
        }
        
        return false;        
    }

    //asking how the user wants to bet
    public static void spinTheWheel(RouletteWheel rouletteWheel){
        Scanner keyboard = new Scanner(System.in);
        
        boolean confirmation = false;
        int betNumber=0;
        while(!confirmation){
            System.out.println("Input a valid number that you would like to bet on (0-36)");
            betNumber = keyboard.nextInt();
            if(betNumber >=0 && betNumber <37){
                confirmation = true;
            }
        }
        

        rouletteWheel.spin();
        System.out.println("the number was... " + rouletteWheel.getValue());

        //win or lose messages
        if(betNumber == rouletteWheel.getValue()){
            win(rouletteWheel);
        }   
        else{
            lost(rouletteWheel); 
        }
    }

    //helper method for winning message
    private static void win(RouletteWheel rouletteWheel){
        double newTotal = rouletteWheel.getBettingAmount()*35 + rouletteWheel.getBetMoney();
        rouletteWheel.setBetMoney(newTotal);
        System.out.println("Congrats! your new total is: " + rouletteWheel.getBetMoney());
        rouletteWheel.resetBetAmount();
    }

    //helper method for loosing message
    private static void lost(RouletteWheel rouletteWheel){
        double newTotal = rouletteWheel.getBetMoney()-rouletteWheel.getBettingAmount();
        rouletteWheel.setBetMoney(newTotal);
        System.out.println("Your new total is: " + rouletteWheel.getBetMoney());
        rouletteWheel.resetBetAmount();
    }
}