import java.util.Random;
public class RouletteWheel {
    private Random random;
    private int numberOfLastSpin;
    private double betMoney;
    private double bettingAmount;

    public RouletteWheel(){
        this.random = new Random();
        this.numberOfLastSpin = 0;
        this.betMoney = 1000;
        this.bettingAmount=0;
    }

    public void spin(){
        int randomNumber = random.nextInt(37);
        this.numberOfLastSpin = randomNumber;
    }

    public int getValue(){
        return this.numberOfLastSpin;
    }

    public double getBetMoney(){
        return this.betMoney;
    }

    public double getBettingAmount(){
        return this.bettingAmount;
    }

    public void setBettingAmount(double bettingAmount){
        if(bettingAmount <=0){
            throw new IllegalArgumentException("You need a valid betting amount");
        }
        this.bettingAmount = bettingAmount;
    }

    public void setBetMoney(double newTotal){
        if(newTotal <0){
            throw new IllegalArgumentException("Your new bet amount cannot be bellow 0");
        }
        this.betMoney = newTotal;
    }

    public void resetBetAmount(){
        this.bettingAmount=0;
    }
}